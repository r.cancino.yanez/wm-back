const jwt = require('jsonwebtoken');

const validatoken = (req, res, next) => {
    const token = req.header('token');
    if( !token ){
        return res.status(401).json({
            flgok: false,
            mje: 'Usuario no autenticado'
        });
    }

    try{
        const usrid = jwt.verify(token,process.env.JWT_SECRET);
        console.log(usrid);
    }
    catch(error){
        return res.status(401).json({
            flgok: false,
            mje: 'Usuario no autenticado'
        });
    }

    next();
}

module.exports = { validatoken };