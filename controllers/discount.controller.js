const Discount = require('../modelos/discount.model');

// const {nuevoToken} = require('../utils/jwt');


const getDiscounts = async(req, res) => {
    const dis = await Discount.find();
    // const token = await nuevoToken('123');
    res.json({
        ok: true,
        data: dis
        // token: token
    });
}


module.exports = {getDiscounts};