const Producto = require('../modelos/product.model');

const getProductos = async(req, res) => {
    const prd = await Producto.find();
    res.json({
        ok:true,
        data: prd
    });
}
module.exports = {getProductos};