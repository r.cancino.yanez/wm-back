const mongoose = require('mongoose');
require('dotenv').config();

const dbConn = async() => {

    try{
        mongoose.connect(process.env.CONNSTR, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('online');
    }
    catch (error){
        throw new Error('Error al iniciar BD');
    }

}
module.exports = {
    dbConn
}