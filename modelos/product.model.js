const {Schema, model} = require('mongoose');

const ProducSchema = Schema({
    id: {
        type: Number,
        required: true,
        unique: true
    },
    brand: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true,
    }
});
module.exports = model('Products', ProducSchema);
