const {Schema, model} = require('mongoose');
const DiscountSchema = Schema({
    id: {
        type: Number,
        required: true,
        unique: true
    },
    brand: {
        type: String,
        required: true,
    },
    threshold: {
        type: Number,
        required: true,
    },
    discount: {
        type: Number,
        required: true,
    }
});

module.exports = model('Discounts', DiscountSchema);