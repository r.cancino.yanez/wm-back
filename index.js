const express = require('express');
const {dbConn} = require('./db/config')
const cors = require('cors');


const app = express();

app.use(cors());

dbConn();

app.use('/wm-api/productos', require('./routes/productos.route'));
app.use('/wm-api/discount', require('./routes/discount.route'));

app.listen(3500);