const jwt = require('jsonwebtoken');

const nuevoToken = (usrid)=>{
    return new Promise((resolve, reject) => {
        const payload = {usrid};
        jwt.sign(payload, process.env.JWT_SECRET,{
            expiresIn: '1h'
        },(err,token) => {
            if(err){
                reject('No se generó JWT');
            }
            else{
                resolve( token );
            }
        });
    });
}

module.exports = {nuevoToken};
