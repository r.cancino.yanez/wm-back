const { Router } = require('express');

const { getProductos } = require('../controllers/producto.controller');
const {validatoken} = require('../middlewares/validar-token');
const router = Router();

// router.post('/',validatoken, getProductos);
router.post('/', getProductos);


module.exports = router;