const { Router } = require('express');
const { getDiscounts } = require('../controllers/discount.controller');
// const {validatoken} = require('../middlewares/validar-token');

const router = Router();
// router.post('/',validatoken, getDiscounts);
router.post('/', getDiscounts);


module.exports = router;